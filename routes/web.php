<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});


Route::get('/lista-empleados' , [App\Http\Controllers\EmployedController::class , 'index'])
->name("EmployedsList");

Route::get('/crear-empleados' , [App\Http\Controllers\EmployedController::class , 'create'])
->name("CreateEmployeds");

Route::post('/store-empleados' , [App\Http\Controllers\EmployedController::class , 'store'])
->name("StoreEmpleados");

Route::get('/editar-empleados' , [App\Http\Controllers\EmployedController::class , 'edit'])
->name("EditEmpleados");

Route::post('/update-empleados' , [App\Http\Controllers\EmployedController::class , 'update'])
->name("UpdateEmpleados");
