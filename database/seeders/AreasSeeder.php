<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Area\Area;
class AreasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       Area::create(
           ["nombre" => "Administración"]
        );

        Area::create(
            ["nombre" => "Ventas"],
            
         );

         Area::create(
            
            ["nombre" => "Calidad"],
            
         );

         Area::create(
            
            
            ["nombre" => "Producción"]
         );
    }
}
