<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Roles\Rol;
class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Rol::create(
            ["nombre" => "Profesional de proyectos - Desarrollador"]
         );

         Rol::create(
            
            ["nombre" => "Gerente estratégico"]
         );

         Rol::create(
            
            ["nombre" => "Auxiliar administrativo"]
         );
    }
}
