<?php

namespace App\Http\Controllers;

use App\Models\Employed\Employed;
use App\Models\Roles\Rol;
use App\Models\Area\Area;
use Illuminate\Http\Request;
use Inertia\Inertia;
class EmployedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Inertia::render('Employeds/EmployedsList', [
            'employeds' => Employed::latest()->get()
        ]);
    }


    public function create()
    {
        return Inertia::render('Employeds/EmployedsCreate' , 
        [
            'areas' => Area::latest()->get(),
            'roles' => Rol::latest()->get()
        ]);
    }

   /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Note  $note
     * @return \Illuminate\Http\Response
     */
    public function edit(Employed $employed)
    {
        return Inertia::render('Employeds/EmployedsEdit', compact('Employed'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nombre' => 'required',
            'email' => 'required',
            'sexo' => 'required',
            'area_id' => 'required',
            'boletin',
            'descripcion' => 'required'
        ]);
        
        Employed::create($request->all());
        return redirect()->route("EmployedsList");
        
        

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Employed\Employed  $employed
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Employed::find($id);
    }

    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Employed\Employed  $employed
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employed $employed)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Employed\Employed  $employed
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employed $employed)
    {
        //
    }
}
